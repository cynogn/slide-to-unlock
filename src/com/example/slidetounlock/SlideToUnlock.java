package com.example.slidetounlock;

import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.RelativeLayout;

public class SlideToUnlock extends Activity {
	Button slideButton;
	private int count = 0;
	android.widget.RelativeLayout.LayoutParams params;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_slide_to_unlock);
		slideButton = (Button) findViewById(R.id.button1);
		params = (RelativeLayout.LayoutParams) slideButton.getLayoutParams();
		slideButton.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {

				switch (event.getAction()) {
				case MotionEvent.ACTION_MOVE:
					count = count + 2;
					params.setMargins(count, 5, 5, 5);
					slideButton.setLayoutParams(params);
					break;
				case MotionEvent.ACTION_UP:
					count = 0;
					params.setMargins(count, 5, 5, 5);
					slideButton.setLayoutParams(params);
				default:
					break;
				}

				return false;
			}
		});

	}
}
